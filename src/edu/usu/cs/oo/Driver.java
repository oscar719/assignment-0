package edu.usu.cs.oo;

public class Driver {
	
	public static void main(String[] args)
	{
		//Student student = new Student("Chris Thatcher", "AOneMillion", new Job("Dentist", 100000, new Company()));
		
		
		/*
		 * Instantiate an instance of Student that includes your own personal information
		 * Feel free to make up information if you would like.
		 */
		
		Student studentA = new Student("Chun Kwan Ng", "A01872123", new Job("Programmer", 99999, new Company("Best Softwave", new Location("100 main st","84341","Logan","UT"))));
		
		System.out.println(studentA);
		
		/*
		 * Print out the student information. 
		 */
	}

}